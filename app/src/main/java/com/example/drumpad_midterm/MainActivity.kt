package com.example.drumpad_midterm

import android.media.MediaPlayer
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private var mediaPlayer: MediaPlayer? = null



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        button1.setOnClickListener{
            var mediaPlayer1 = MediaPlayer.create(this, R.raw.kick1)
            mediaPlayer1?.start()
        }

        button2.setOnClickListener{
            var mediaPlayer2 = MediaPlayer.create(this, R.raw.snare)
            mediaPlayer2?.start()
        }

        button3.setOnClickListener{
            var mediaPlayer3 = MediaPlayer.create(this, R.raw.cl_hihat)
            mediaPlayer3?.start()
        }

        button4.setOnClickListener{
            var mediaPlayer4 = MediaPlayer.create(this, R.raw.crashcym)
            mediaPlayer4?.start()
        }

        button5.setOnClickListener{
            var mediaPlayer5 = MediaPlayer.create(this, R.raw.cowbell)
            mediaPlayer5?.start()
        }

        button6.setOnClickListener{
            var mediaPlayer6 = MediaPlayer.create(this, R.raw.open_hh)
            mediaPlayer6?.start()
        }

        button7.setOnClickListener{
            var mediaPlayer7 = MediaPlayer.create(this, R.raw.handclap)
            mediaPlayer7?.start()
        }

        button8.setOnClickListener{
            var mediaPlayer8 = MediaPlayer.create(this, R.raw.hightom)
            mediaPlayer8?.start()
        }

        button9.setOnClickListener{
            var mediaPlayer9 = MediaPlayer.create(this, R.raw.tom1)
            mediaPlayer9?.start()
        }

        button10.setOnClickListener{
            var mediaPlayer10 = MediaPlayer.create(this, R.raw.kick1)
            mediaPlayer10?.start()
        }

        button11.setOnClickListener{
            var mediaPlayer11 = MediaPlayer.create(this, R.raw.snare)
            mediaPlayer11?.start()
        }

        button12.setOnClickListener{
            var mediaPlayer12 = MediaPlayer.create(this, R.raw.cl_hihat)
            mediaPlayer12?.start()
        }



    }


}
